#!/bin/bash
#
# could install from local path
# the local path should specify the parent directory
RPMDIR=${1:-""}

function service_is_active
{
	service=$1
	for i in {1..60};do
		state=$(systemctl is-active $service)
		if (($state == "active"));then
			break
		else
			sleep 1
		fi
	done
}

function Install_Pouch
{
	if grep -qi "ubuntu" /etc/issue ; then
		install_on_ubuntu
	else
		install_on_centos
	fi
}

function install_on_ubuntu
{
	systemctl stop pouch
	yes | apt-get purge pouch
	rm -rf /var/lib/pouch
	
	if [[ $RPMDIR != "" ]] ;then
		dpkg -i $RPMDIR/package/deb/pouch*.deb
        rm -f /usr/local/bin/pouch
        rm -f /usr/local/bin/pouchd

		ln -s /usr/bin/pouch /usr/local/bin/pouch
		return $?
	fi
	yes | apt-get install lxcfs
	apt-get install curl apt-transport-https ca-certificates software-properties-common
	curl -fsSL http://mirrors.aliyun.com/opsx/pouch/linux/debian/opsx@service.alibaba.com.gpg.key | sudo apt-key add -
	apt-key fingerprint 439AE9EC |grep B615
	if (( $? != 0 )) ;then
		echo "Install key fail"
		exit 1
	fi
	add-apt-repository "deb http://mirrors.aliyun.com/opsx/pouch/linux/debian/ pouch stable"
	apt-get update
	apt-get install -y pouch
	if (( $? != 0 )) ;then
		echo "Install fail"
		exit 1
	fi
	ln -s /usr/bin/pouch /usr/local/bin/pouch
}

function install_on_centos
{
	systemctl stop pouch
	yum remove -y pouch
	rm -rf /var/lib/pouch

	if [[ $RPMDIR != "" ]] ;then
		rpm  -ivh $RPMDIR/package/rpm/pouch*.rpm
		if (( $? != 0 )) ;then
			echo "Failed to install pouch RPM!"
			exit 1
		else
			return 0
		fi
	fi

	yum install -y yum-utils

	yum-config-manager \
		    --add-repo \
			http://mirrors.aliyun.com/opsx/opsx-centos7.repo
	yes | yum update

	yum install -y pouch 
	if (( $? != 0 )) ;then
		echo "Install fail"
		exit 1
	fi
	#systemctl start pouch
        # make sure pouch can be started completely
        #service_is_active pouch
}

Install_Pouch
