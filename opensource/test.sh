#!/bin/bash

export POUCH_HELLOWORLDREPO="registry.hub.docker.com/hello-world"
export POUCH_HELLOWORLDTAG="latest"
export POUCH_BUSYBOXREPO="registry.hub.docker.com/busybox"
export POUCH_BUSYBOXTAG="latest"

set -e 

# the directory of pouch-CI-asis
TMP=${1:?"must required"}

commit=master
git config --global http.postBuffer 12M

echo "Clean Env"
pkill pouchd || echo ""
pkill containerd || echo ""

mv /var/lib/pouch /var/lib/pouchold$(date +'%y%m%d%h%M%s') || echo ""
rm -rf /var/lib/pouchold$(date +'%y%m%d%h%M%s') || echo "warning: remove file failed"

# fix:fuse: bad mount point `/var/lib/lxc/lxcfs': Transport endpoint is not connected
# only need to install on centos
if ! grep -qi "ubuntu" /etc/issue ; then
    yes |yum install fuse.x86_64
fi

fusermount -u /var/lib/lxc/lxcfs || echo "fail to run fusermount"

echo "$commit"

echo "clone pouch and checkout to test commit:$commit"
cd $TMP
mkdir -p src/github.com/alibaba
cd src/github.com/alibaba
if [ -d pouch ] ; then
    rm -rf pouch
fi
git clone https://github.com/alibaba/pouch.git
cd pouch

# 
rm -f /var/run/docker/plugins/local-persist.sock || echo ""

# local-persist is a volume plugin
echo "Try installing local-persist"
cp $TMP/src/plugin/local-persist-linux-amd64 \
        /usr/local/bin/local-persist
chmod +x /usr/local/bin/local-persist

# start local-persist
echo "start local-persist volume plugin"
local-persist > $TMP/volume.log 2 >&1 &

# enable lxcfs first
yes | cp ./hack/package/rpm/service/lxcfs.service  /usr/lib/systemd/system/lxcfs.service
systemctl enable lxcfs || echo "enable lxcfs failed"
systemctl start lxcfs || echo "start lxcfs failed"

if [ "$commit" != "master" ] ; then
	git checkout $commit || exit 1
fi

#
# workaround libnetwork
#
hack/build pre

BUILDPATH=/tmp/pouchbuild
export GOPATH=$BUILDPATH/src/github.com/docker/libnetwork/Godeps/_workspace:$TMP
export GOROOT=/usr/local/go
export PATH=$GOROOT/bin:$PATH
# install go1.9 and set GOPATH for cri-test
if ! go version ; then
	cd $TMP/pouch-CI-asis/src/golang
	tar -xzf go1.9.1.linux-amd64.tar.gz
	mv go /usr/local/go || echo "fail to move go"
	go version || echo "OOPS: install go1.9.1 failed"
	cd -
fi

LOG="/tmp/pouchdlog"
pouchd -D --enable-lxcfs --lxcfs /usr/bin/lxcfs --enable-cri --cri-version v1alpha2 >$LOG 2>&1 &
sleep 10
if ! pouch version ; then
	cat $LOG
	exit 1
fi
pouch rm -f $(pouch ps -qa) || echo "remove pouch container failed"
pouch rmi $(pouch images -q) || echo "remove pouch images failed"
pouch pull $POUCH_BUSYBOXREPO:$POUCH_BUSYBOXTAG


set +e
FAIL=0
make unit-test >$TMP/unit.log 2>&1 
if (( $? != 0 )); then
	echo "unit-test failed"
	(( FAIL++ ))
fi

cd test;
go test -timeout 90m -check.v >$TMP/integration.log 2>&1
if (( $? != 0 )); then
	echo "integration-test failed"
	(( FAIL++ ))
fi

cd ..
hack/cri-test/test-cri.sh > $TMP/cri.log 2>&1
if (( $? != 0 )); then
	echo "cri-test failed"
	(( FAIL++ ))
fi

cat $TMP/integration.log
cat $TMP/cri.log
