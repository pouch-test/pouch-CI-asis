
if grep -qi "ubuntu" /etc/issue ; then
yes | apt-get install \
apt-transport-https \
 ca-certificates \
       curl \
            software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88 |grep -q "9DC8"  || exit 1

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
         stable"

yes | apt-get update

yes | apt-get install docker-ce

fi
